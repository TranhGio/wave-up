New in 3.0.12
★ Fix lock option for paid versions. There were compatibility problems when several versions where installed.

New in 3.0.11
★ Fix small bugs while writing logs and reporting issues.

New in 3.0.10
★ Fix small "gratitude" bug.

New in 3.0.9
★ Work-around for small bug showing a "gratitude" dialog that shouldn't be shown under some circumstances.
